package mx.yamil.hr.dao;

import java.util.List;

import mx.yamil.hr.dto.EmployeesDTO;

public interface EmployeesDAO {
	public EmployeesDTO getEmployeeById(Long idEmployees);
	public List<EmployeesDTO> getAllEmployees();
}
