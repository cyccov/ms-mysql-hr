package mx.yamil.hr.controller;

import static mx.yamil.hr.utils.HrConstants.API_PATH_BASE;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mx.yamil.hr.app.EmployeesApp;
import mx.yamil.hr.dto.EmployeesDTO;
import mx.yamil.hr.handler.ResponseHandler;

@RestController
@RequestMapping(value = API_PATH_BASE)
public class EmployeesController {

	@Autowired
	ResponseHandler responseHandler;
	
	@Autowired
	EmployeesApp employeesApp;
	
	@GetMapping("/employees")
	public ResponseEntity<Object> getEmployees() {
		long initialTime = System.currentTimeMillis();
		List<EmployeesDTO> employees = employeesApp.getAllEmployees();
		return responseHandler.generarRespuestaOK(initialTime, employees);
	}
	
	
	
	
}
