package mx.yamil.hr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsMysqlHrApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsMysqlHrApplication.class, args);
	}

}
