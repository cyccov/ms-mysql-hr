package mx.yamil.hr.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "DEPARTMENTS")
public class DepartmentsEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "DEPARTMENT_ID")
	private Long idDepartment;
	
	@Column(name = "DEPARTMENT_NAME")
	private String departmentName;
//	private Manager manager;
//	private Location location;
	
	public Long getIdDepartment() {
		return idDepartment;
	}
	public void setIdDepartment(Long idDepartment) {
		this.idDepartment = idDepartment;
	}
	public String getDepartmentName() {
		return departmentName;
	}
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}
	
	
}
