package mx.yamil.hr.controller;
import static mx.yamil.hr.utils.HrConstants.API_PATH_BASE;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mx.yamil.hr.handler.ResponseHandler;
import mx.yamil.hr.repository.DepartmentsRepository;

@RestController
@RequestMapping(value = API_PATH_BASE)
public class DepartmentsController {
	
	@Autowired
	ResponseHandler responseHandler;
	
	@Autowired
	DepartmentsRepository departmentsRepository;
	
	@GetMapping("/departments")
	public ResponseEntity<Object> getAllDepartments(){
		long initialTime = System.currentTimeMillis();
		return responseHandler.generarRespuestaOK(initialTime, responseHandler);
	}
	
	@GetMapping("/departments/{idDepartment}")
	public ResponseEntity<Object> getDepartmentById(@PathVariable(value = "idDepartment") long idDepartment) {
		long initialTime = System.currentTimeMillis();		
		return responseHandler.generarRespuestaOK(initialTime, responseHandler);
	}
}
