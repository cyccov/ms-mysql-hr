package mx.yamil.hr.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import mx.yamil.hr.dao.EmployeesEntity;

@Repository
public interface EmployeesRepository extends JpaRepository<EmployeesEntity, Long>{
	@Query("SELECT e FROM EmployeesEntity e")
	public List<EmployeesEntity> getAllEmployees();
	
	@Query("Select e.idEmployee FROM EmployeesEntity e WHERE e.idEmployee = ?1")
	public EmployeesEntity getEmployeeById(Long idEmployees);
}
