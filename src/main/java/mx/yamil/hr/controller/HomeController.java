package mx.yamil.hr.controller;

import static mx.yamil.hr.utils.HrConstants.API_PATH_BASE;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mx.yamil.hr.handler.ResponseHandler;

@RestController
@RequestMapping(value = API_PATH_BASE)
public class HomeController {	
	@Autowired
	ResponseHandler responseHandler;
	
	@GetMapping(path = "/home")
	public ResponseEntity<Object> getHome(){
		String mensaje = "ms-mysql-hr v 1.0.0";
		Map<String, Object> pruebaLista = new HashMap();
		List<String> nombres = new ArrayList();
		Map<String, Object> submap = new HashMap();				
		nombres.add("Aleida");
		nombres.add("Dafne");
		nombres.add("Christian Yamil");
		pruebaLista.put("listaNombres", nombres);
		long time = System.currentTimeMillis();
		return ResponseHandler.generarRespuestaOK(time, pruebaLista);
	}
	
}