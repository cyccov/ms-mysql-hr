package mx.yamil.hr.handler;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class ResponseHandler {
	private final static String MENSAJE_OK = "Respuesta exitosa.";
	private final static String MENSAJE_ERROR = "";
		
	public static ResponseEntity<Object> generarRespuestaOK(long responseTime, Object response) {
		Map<String, Object> responseMap = new HashMap<>();
		if (response instanceof Map) {
			responseMap.put("datos", response);
			responseMap.put("mensaje", MENSAJE_OK);
			responseMap.put("status", HttpStatus.OK);		
			responseMap.put("tiempoRespuesta", (String.valueOf(System.currentTimeMillis() - responseTime)+"ms"));			
			return new ResponseEntity<>(responseMap, HttpStatus.OK);
		} else {
			responseMap.put("datos", response);		
			responseMap.put("mensaje", MENSAJE_OK);
			responseMap.put("status", HttpStatus.OK);		
			responseMap.put("tiempoRespuesta", (String.valueOf((System.currentTimeMillis() - responseTime)/1000)+" ms"));
			return new ResponseEntity<>(responseMap, HttpStatus.OK);
		}
	}
}
