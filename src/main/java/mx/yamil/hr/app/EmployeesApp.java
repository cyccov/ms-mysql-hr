package mx.yamil.hr.app;

import java.util.List;

import mx.yamil.hr.dto.EmployeesDTO;

/**
 * Logica para Entity Employees
 * */

public interface EmployeesApp {

	public List<EmployeesDTO> getAllEmployees();
	public EmployeesDTO getEmployeeById(Long idEmployee);
	
}
