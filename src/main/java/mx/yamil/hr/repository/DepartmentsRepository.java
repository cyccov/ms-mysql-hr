package mx.yamil.hr.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import mx.yamil.hr.dao.DepartmentsEntity;

@Repository
public interface DepartmentsRepository extends JpaRepository<DepartmentsEntity, Long>{
	
	@Query(value = "SELECT d.departmentName FROM DepartmentsEntity d")
	public DepartmentsEntity getDepartmentById(Long idDepartment);
}
