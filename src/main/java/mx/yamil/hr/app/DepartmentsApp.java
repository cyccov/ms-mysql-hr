package mx.yamil.hr.app;

import mx.yamil.hr.dto.DepartmentsDTO;

public interface DepartmentsApp {
	public DepartmentsDTO getDepartmentById(Long idDepartment);
}
